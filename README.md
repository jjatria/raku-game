## NAME

Mini::Game - experiments with Raku and SDL2

## SYNOPSIS

```raku
use Mini::Game;
Mini::Game.run;
```

## DESCRIPTION

Mini::Game is a minimal game project I use to experiment with Raku and SDL2.

## AUTHOR

José Joaquín Atria <jjatria@gmail.com>

## ACKNOWLEDGEMENTS

The sprites come from 0x72's [Dungeon Tileset II].

## COPYRIGHT AND LICENSE

Copyright 2020 José Joaquín Atria

This library is free software; you can redistribute it and/or modify it under
the Artistic License 2.0.

[Dungeon Tileset II]: https://0x72.itch.io/dungeontileset-ii
