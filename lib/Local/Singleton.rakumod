unit module Local::Singleton;

role MetamodelX::SingletonHOW {
    my $bypass = set << new bless BUILDALL BUILD dispatch:<!> >>;

    method _rw_wrapper($self: |c) is rw { callwith( $self // $self.new, |c ) }
    method _ro_wrapper($self: |c)       { callwith( $self // $self.new, |c ) }

    method compose(Mu $obj) {
        callsame;
        for $obj.^method_table.kv -> $name, $code {
            next if $bypass{$name};

            # This is horrid but callwith needs to see the 'rw'
            # when the wrapper is compiled it seems
            $code.wrap: self.^find_method(
                $code.rw ?? '_rw_wrapper' !! '_ro_wrapper' );
        }
    }
}

role Singleton {
    my $single;
    multi method new(|c) { $single //= callsame }
}

multi sub trait_mod:<is>(Mu:U $doee, :$Singleton!) is export {
    $doee.HOW does MetamodelX::SingletonHOW;
    $doee.^add_role(Singleton);
}
