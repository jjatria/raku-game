unit package SDL2::Image;

use NativeCall;
use SDL2::Raw;

our constant IMG_INIT_JPG  is export = 0x00000001; # JPG
our constant IMG_INIT_PNG  is export = 0x00000002; # PNG
our constant IMG_INIT_TIF  is export = 0x00000004; # TIF
our constant IMG_INIT_WEBP is export = 0x00000008; # WebP

sub IMG_Init (int32 $flag)
    returns int32
    is export
    is native('SDL2_image') {*}

sub IMG_LoadTexture ( SDL_Renderer $renderer, Str $path )
    returns SDL_Texture
    is export
    is native('SDL2_image') {*}

sub IMG_GetError ()
    returns str
    is export
    is native('SDL2_image') {*}

