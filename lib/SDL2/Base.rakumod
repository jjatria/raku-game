use SDL2;
use SDL2::Raw;

sub EXPORT {
    SDL2::Raw::EXPORT::ALL::
}

unit module SDL2::Base;

use NativeCall;
sub SDL_WasInit (uint32 $flags)
    returns uint32
    is export
    is native('SDL2') {*}

sub SDL_InitSubSystem (uint32 $flags)
    returns uint32
    is export
    is native('SDL2') {*}

sub SDL_ShowCursor (int32 $toggle)
    returns int32
    is export
    is native('SDL2') {*}

sub SDL_Delay (uint32 $ms)
    is export
    is native('SDL2') {*}

sub SDL_QueryTexture (
    SDL_Texture $texture,
    uint32 $format is rw,
    int32  $access is rw,
    int32  $w      is rw,
    int32  $h      is rw,
)
    returns int32
    is export
    is native('SDL2') {*}

class SDL_Joystick is export is repr('CPointer') { }

class SDL_JoyAxisEvent is export is repr('CStruct') is rw {
    has uint32 $.type;
    has uint32 $.timestamp;
    has int32  $.which;
    has uint8  $.axis;
    has int32  $!value;

    method value { my int16 $foo = $!value }
}

sub SDL_NumJoysticks ()
    returns int32
    is export
    is native('SDL2') {*}

sub SDL_JoystickOpen( int32 $idx )
    returns SDL_Joystick
    is export
    is native('SDL2') {*}

sub SDL_JoystickClose( SDL_Joystick $joystick )
    is export
    is native('SDL2') {*}

sub SDL_JoystickOpened( int32 $idx )
    returns int32
    is export
    is native('SDL2') {*}

sub SDL_JoystickEventState( int32 $toggle )
    returns int32
    is export
    is native('SDL2') {*}
