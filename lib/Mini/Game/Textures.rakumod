use Local::Singleton;
unit class Mini::Game::Textures is Singleton;

use SDL2::Base;
use SDL2::Image;

has SDL_Texture %!map;

method load (
    SDL_Renderer $renderer,
    Str:D        $path,
    Str:D        $id,
    --> Bool
) {
    note "Loading $path...";
    if IMG_LoadTexture( $renderer, $path ) -> $texture {
        %!map{$id} = $texture;
        return True;
    }

    note 'Error: ' ~ IMG_GetError;
    return False;
}

method draw (
    SDL_Renderer $renderer,
    Str:D        $id,
    Complex:D    $pos,
    Complex:D    $size,
                :$flip = RENDERER_FLIP_NONE,
    --> Nil
) {
    die "No such texture: $id" unless %!map{$id};

    SDL_RenderCopy(
        $renderer,
        %!map{$id},
        SDL_Rect.new( 0,  0,  $size.re, $size.im ), # source
        SDL_Rect.new( $pos.re, $pos.im, $size.re, $size.im ), # destination
    );
}

method draw-frame (
    SDL_Renderer $renderer,
    Str:D        $id,
    Complex:D    $pos,
    Complex:D    $size,
    Int:D        $row,
    Int:D        $frame,
                :$flip = RENDERER_FLIP_NONE,
    --> Nil
) {
    die "No such texture: $id" unless %!map{$id};

    SDL_RenderCopy(
        $renderer,
        %!map{$id},
        SDL_Rect.new( $size.re * $frame, $size.im * $row, $size.re, $size.im ), # source
        SDL_Rect.new( $pos.re,           $pos.im,         $size.re, $size.im ), # destination
    );
}

method destroy of Nil {
    Nil;
}
