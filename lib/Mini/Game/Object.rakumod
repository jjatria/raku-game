unit class Mini::Game::Object;

use SDL2::Raw;
use Mini::Game::Textures;
use Mini::Game::Vector;

has Complex $!pos;
has Complex $!size;
has Complex $.delta is rw .= new: 0, 0;

has Str $!texture-id is built;
has Int $.frame is rw = 0;
has int $!row   = 0;

submethod TWEAK ( :$x, :$y, :$w, :$h ) {
    $!pos  = Mini::Game::Vector.new( $x, $y );
    $!size = Mini::Game::Vector.new( $w, $h );
}

method dx ($this:) is rw {
    Proxy.new(
        FETCH => method () { $this.delta.re },
        STORE => method ($new) {
            $this.delta = Mini::Game::Vector.new: $new, $this.delta.im
        },
    );
}

method dy ($this:) is rw {
    Proxy.new(
        FETCH => method () { $this.delta.im },
        STORE => method ($new) {
            $this.delta = Mini::Game::Vector.new: $this.delta.re, $new
        },
    );
}

method clean of Nil { }

method update of Nil {
    $!pos += $!delta / 10_000;
    Nil;
}

method draw ( SDL_Renderer $rdr ) of Nil {
    # note "Drawing object at { $!pos.reals.join: ':' }";
    Mini::Game::Textures.draw-frame(
        $rdr,
        $!texture-id,
        $!pos,
        $!size,
        $!row,
        $!frame,
    );
}
