use Mini::Game::Object;
unit class Mini::Game::Player is Mini::Game::Object;

use SDL2::Raw;

method update of Nil {
    $.frame = ( ( SDL_GetTicks / 100 ) % 4 ).Int;
    nextsame;
}
