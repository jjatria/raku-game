unit class Mini::Game::Vector is Complex;

method length of Numeric { sqrt $.re * $.re + $.im * $.im }

method normalise of ::?CLASS { self * $.length * -1 }
