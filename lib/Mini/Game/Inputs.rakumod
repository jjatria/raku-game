use Local::Singleton;
unit class Mini::Game::Inputs is Singleton;

use SDL2::Base;

has SDL_Joystick @!joysticks;

method has-joysticks of Bool { so @!joysticks }

method update of Nil {
    use NativeCall;
    state $event = SDL_Event.new;

    while SDL_PollEvent($event) {
        given $event.type {
            when QUIT { $*MINI-GAME.quit }
            when JOYAXISMOTION {
                my $e = nativecast( SDL_JoyAxisEvent, $event );

                given $e.axis {
                    when 0 { # Left joystick left / right
                        # @!objects[0].dx = $e.value.&delta;
                    }

                    when 1 { # Left joystick up / down
                        # @!objects[0].dy = $e.value.&delta;
                    }

                    when 2 { # Left shoulder trigger

                    }

                    when 3 { # Right joystick left / right
                        # @!objects[1].dx = $e.value.&delta;
                    }

                    when 4 { # Right joystick up / down
                        # @!objects[1].dy = $e.value.&delta;
                    }

                    when 5 { # Right shoulder trigger

                    }

                    default {
                        dd $e;
                    }
                }
            }
        }
    }

    Nil;
}

method destroy of Nil {
    while @!joysticks.shift -> $joystick {
        SDL_JoystickClose($joystick);
    }

    Nil;
}

method init-joysticks of Nil {
    SDL_InitSubSystem(JOYSTICK) unless SDL_WasInit(JOYSTICK);

    for 1 .. SDL_NumJoysticks() -> $i {
        @!joysticks.push: SDL_JoystickOpen( $i - 1 )
            or die "Could not open joystick: { SDL_GetError() }";

        LAST {
            SDL_JoystickEventState(1);
            note "Initialised { @!joysticks.elems } controllers";
        }
    }

    Nil;
}
