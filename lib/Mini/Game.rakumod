unit class Mini::Game;

use TOML::Thumb;
use SDL2::Base;
use SDL2::Image;
use Mini::Game::Inputs;
use Mini::Game::Textures;
use Mini::Game::Player;

has                    %!config = 'config.toml'.IO.slurp.&from-toml;
has Promise            $!done .= new;
has SDL2::Renderer     $!render;
has SDL2::Window       $!window;
has Mini::Game::Object @!objects;

constant FPS = 60;
constant DELAY-TIME = 1000 / FPS;

submethod TWEAK (
    Str :$title  = %!config<window><title>  // 'No Title',
    Int :$width  = %!config<window><width>  // 640,
    Int :$height = %!config<window><height> // 480,
) {
    if SDL_Init(VIDEO) {
        die "couldn't initialize SDL2: { SDL_GetError }";
    }

    $!window = SDL2::Window.new: :$title, :$width, :$height, :flags(OPENGL);
    $!render = SDL2::Renderer.new($!window);

    SDL_ShowCursor(SDL2::Raw::SDL_DISABLE);
    IMG_Init( IMG_INIT_PNG +| IMG_INIT_JPG );

    for %!config<entities>.kv -> $id, %data {
        with %data<sheet> -> $sheet {
            Mini::Game::Textures.load(
                $!render.renderer,
                $sheet,
                $id,
            ) or die;
        }
    }

    @!objects.push: Mini::Game::Player.new(
        texture-id => 'player',
        x => 0,  y => 0,
        w => 16, h => 28,
    );

    @!objects.push: Mini::Game::Player.new(
        texture-id => 'player',
        x => 100,  y => 100,
        w => 16, h => 28,
    );

    Mini::Game::Inputs.init-joysticks;
}

method DESTROY of Nil {
    try $!render.destroy;
    try $!window.destroy;
    try Mini::Game::Inputs.destroy;
    try Mini::Game::Textures.destroy;
    SDL_Quit;

    Nil;
}

method run of Nil {
    until $!done {
        $.handle-events;
        $.update;
        $.render;

        # Constant frame-rate
        my $t = SDL_GetTicks - ENTER SDL_GetTicks;
        SDL_Delay( ( DELAY-TIME - $t ).Int ) if $t < DELAY-TIME;
    }
    Nil;
}

method handle-events of Nil {
    Mini::Game::Inputs.update;
    Nil;
}

method update of Nil {
    .update for @!objects;
    Nil;
}

method render of Nil {
    with $!render {
        .clear;

        for @!objects -> $o {
            $o.draw: .renderer;
        }

        .present;
    }
    Nil;
}

method quit of Nil {
    $!done.keep if $!done.status ~~ Planned;
    Nil;
}
